# Cat Virtual Price List

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.

## Installation

Project's default package manager is [yarn](https://yarnpkg.com/en/docs/install), tho others like `npm` can be used.

```
yarn install
```

## Development server

Run `yarn run start` for a dev server. Navigate to `http://localhost:8888/`. The app will automatically reload if you change any of the source files.

## Build

Run `yarn run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running integration tests

Run `yarn run e2e` to execute the end-to-end tests via [Cypress](https://www.cypress.io/).

## Generate docs

Run `yarn run docs` to generate documentation via [Compodoc](https://compodoc.app/). Docs will be served on `http://localhost:8887`
