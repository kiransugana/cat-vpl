describe('140M3 Product Configuration', () => {
  beforeEach(()=> {
    cy.visit('/');
  });

  it('should open 140M3 detail page when clicking on "NEW CONFIGURATION"', ()=> {
    cy.get('.configuration-search__action .button__primary').click();
    cy.url().should('include', '/product/140M3');
  });

  it('should show modal when selcting 140M3 MOTOR GRADER with 3 elements changing', ()=> {
    cy.visit('/product/140M3');
    cy.get('#qs-140M3-CFGM1LAN .configuration-option__row:first .configuration-option__row--checkbox').click();
    cy.get('.option-modal').should('exist');
    cy.get('.option-modal .option-table tbody').children().should('have.length', 3);
  });
});