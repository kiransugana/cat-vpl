import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: '../../views/home/home.module#HomeModule',
        data: {
          title: 'Home'
        }
      },
      {
        path: 'product',
        loadChildren: '../../views/product/product.module#ProductModule',
        data: {
          title: 'Products'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
