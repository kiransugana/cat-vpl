import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { HeaderComponent } from './header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as fromState from '../../state';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [MainComponent, HeaderComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    FontAwesomeModule,
    StoreModule.forFeature('product', fromState.productReducers),
    EffectsModule.forFeature([...fromState.$productEffects])
  ]
})
export class MainModule {
}
