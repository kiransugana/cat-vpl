import { Component } from '@angular/core';
import { INavigationItem } from '../../../models/nav';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  mainNavList: INavigationItem[] = [
    {
      label: 'Project',
      routerLink: '/'
    },
    {
      label: 'Order',
      routerLink: '/'
    },
    {
      label: 'Dealer',
      routerLink: '/'
    },
    {
      label: 'Reports',
      routerLink: '/'
    },
    {
      label: 'Admin',
      routerLink: '/'
    }
  ];
}
