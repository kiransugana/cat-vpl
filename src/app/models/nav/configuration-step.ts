export interface IConfigurationStep {
  label: string;
  icon: string;
}
