export * from './configuration-step';

export interface INavigationItem {
  label: string;
  routerLink: string;
  children?: INavigationItem[];
}
