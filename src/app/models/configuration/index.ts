import { IProduct } from '../product';

export * from './answer';
export * from './answer-reason';
export * from './detail';
export * from './pending-update';
export * from './question';
export * from './question-answer';
export * from './selection.type';
export * from './step.type';

export interface IProductConfig extends IProduct {
  questionIds: string[];
  answerIds?: string[];
}
