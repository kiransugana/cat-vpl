export enum ConfigurationStepType {
  CONFIGURATION = 1,
  ATTACHMENT = 2,
  PRICING = 3
}
