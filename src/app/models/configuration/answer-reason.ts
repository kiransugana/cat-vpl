export interface IAnswerReason {
  answer: string;
  displayName: string;
  question: string;
  type?: 'included' | string;
}
