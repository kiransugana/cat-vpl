export enum SelectionType {
  MUST_SELECT_ONE = 1,
  MUST_SELECT_ONE_OR_MANY = 2,
  MAY_SELECT_ONE = 3,
  MAY_SELECT_ONE_OR_MANY = 4
}
