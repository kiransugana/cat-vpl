import { IConfigQuestion } from './question';
import { IConfigAnswer } from './answer';
import { IProductConfig } from './index';
import { IQuestionAnswer } from './question-answer';

export interface IPendingUpdate {
  questions: IConfigQuestion[];
  answers: IConfigAnswer[];
  config?: IProductConfig;
  toAdd: IQuestionAnswer[];
  toRemove: IQuestionAnswer[];
  question?: IConfigQuestion;
  questionIds?: string[];
  answerIds?: string[];
}
