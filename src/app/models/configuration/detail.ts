import { IConfigQuestion } from './question';

export interface IConfigurationDetail {
  id: string;
  questions: IConfigQuestion[];
  questionIds: string[];
  answerIds?: string[];
}
