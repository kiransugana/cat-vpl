import { IConfigAnswer } from './answer';
import { SelectionType } from './selection.type';

export interface IConfigQuestion {
  ord: number;
  id: string;
  displayName: string;
  seq_no: string;
  answers?: IConfigAnswer[];
  answerIds?: string[];
  selected_value: string[];
  selection_type: SelectionType;
  valid?: boolean;
  tooltipText?: string;
}
