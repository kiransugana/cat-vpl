import { IAnswerReason } from './answer-reason';

export interface IConfigAnswer {
  id: string;
  displayName: string;
  ord: string;
  seq_no: string;
  price: string;
  description?: string;
  notes?: string;
  customerApplication?: string;
  expandDetails?: boolean;
  detailsLoaded?: boolean;
  isDisabled?: boolean;
  isIncluded?: boolean;
  status?: string;
  reason?: IAnswerReason;
}
