export interface IReferenceDocument {
  icon: 'web' | 'pdf' | 'yt';
  label: string;
  url?: string;
}
