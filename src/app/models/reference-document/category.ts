import { IReferenceDocument } from './index';

export interface IReferenceDocumentCategory {
  id?: number;
  title: string;
  hasMore?: boolean;
  referenceDocuments: IReferenceDocument[];
}
