export interface IProduct {
  id: string;
  displayName?: string;
}
