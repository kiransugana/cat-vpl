import { IConfigAnswer, IConfigQuestion, SelectionType } from '../../models/configuration';

export class CustomValidators {
  static QuestionValidator(control: { question: IConfigQuestion, answer: IConfigAnswer }): boolean {
    if (control.answer.isDisabled
    || (control.question.selection_type === SelectionType.MUST_SELECT_ONE && control.question.selected_value.includes(control.answer.id))
    || (control.question.selection_type === SelectionType.MUST_SELECT_ONE_OR_MANY && control.question.selected_value.includes(control.answer.id) && control.question.selected_value.length === 1)) {
      return false;
    } else  if ((control.question.selected_value.length === 0)
      || (control.question.selection_type === SelectionType.MAY_SELECT_ONE || control.question.selection_type === SelectionType.MAY_SELECT_ONE_OR_MANY)) {
      return true;
    }

    return true;
  }
}
