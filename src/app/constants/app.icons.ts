import {
  faCaretDown,
  faUser,
  faSearch,
  faChevronDown,
  IconDefinition,
  faInfo,
  faChevronUp,
  faCheck, faTimes, faEdit
} from '@fortawesome/free-solid-svg-icons';

export const APP_ICONS: IconDefinition[] = [
  faCaretDown,
  faUser,
  faTimes,
  faEdit,
  faSearch,
  faChevronDown,
  faChevronUp,
  faInfo,
  faCheck
];
