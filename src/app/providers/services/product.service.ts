import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAnswerReason, IConfigAnswer, IConfigQuestion, IProductConfig, IQuestionAnswer } from '../../models';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  readonly API_ENDPOINT = `${environment.api}/model`;

  constructor(private http: HttpClient) {
  }

  getProductConfiguration(modelId: string): Observable<{ questions: IConfigQuestion[], answers: IConfigAnswer[], config: IProductConfig }> {
    const url = `${this.API_ENDPOINT}/${modelId}/questions`;

    return this.http.get(url).pipe(
      map((questions: IConfigQuestion[]) => {
        const answerList = [];
        const questionIds = [];
        const answerIds = [];
        const qsList = questions.map(question => {
          question.answerIds = question.answers.map(answer => answer.id);
          answerList.push(...question.answers.map(answer => {
            answer.isDisabled = false;
            answerIds.push(answer.id);
            return answer;
          }));
          question.answers = [];
          question.selected_value = question.selected_value || [];
          question.selection_type = +question.selection_type;
          questionIds.push(question.id);
          return question;
        });

        return <{ questions: IConfigQuestion[], answers: IConfigAnswer[], config: IProductConfig }>{
          questions: qsList,
          answers: answerList,
          config: {
            id: modelId,
            questionIds,
            answerIds
          }
        };
      })
    );
  }

  updateProductConfiguration(data: { modelId: string, question: IConfigQuestion, answer: IConfigAnswer, selectedConfiguration: { [key: string]: string[]; } }): Observable<{ questions: IConfigQuestion[], answers: IConfigAnswer[], config: IProductConfig, toAdd: IQuestionAnswer[], toRemove: IQuestionAnswer[] }> {
    const url = `${this.API_ENDPOINT}/${data.modelId}/questions`;

    const payload = {
      selection: {
        ...data.selectedConfiguration
      },
      actions: [
        {
          target: {
            question: data.question.id,
            answer: data.answer.id
          },
          value: !data.question.selected_value.includes(data.answer.id) // todo: set value check/uncheck
        }
      ]
    };
    return this.http.post(url, payload).pipe(
      map((res: { questions: IConfigQuestion[], toAdd: IQuestionAnswer[], toRemove: IQuestionAnswer[] }) => {
        const answerList = [];
        const questionIds = [];
        const qsList = res.questions.map(question => {
          question.answerIds = question.answers.map(answer => answer.id);
          answerList.push(...question.answers.map(answer => {
            answer.isDisabled = false;
            answer.isIncluded = answer.reason && answer.reason.type === 'included';

            if (answer.isIncluded) {
              answer.status = `Included in ${answer.reason.displayName}`;
            } else {
              answer.status = '';
            }

            return answer;
          }));
          question.answers = [];
          question.selected_value = question.selected_value || [];

          questionIds.push(question.id);
          return question;
        });

        return <{ questions: IConfigQuestion[], answers: IConfigAnswer[], config: IProductConfig, toAdd: IQuestionAnswer[], toRemove: IQuestionAnswer[] }>{
          questions: qsList,
          answers: answerList,
          config: {
            id: data.modelId,
            questionIds
          },
          toAdd: res.toAdd,
          toRemove: res.toRemove
        };
      }),
    );
  }

  getAnswerDetails(data: { modelId: string, questionId: string, answerId: string }): Observable<IConfigAnswer> {
    const url = `${this.API_ENDPOINT}/${data.modelId}/questions/${data.questionId}/answers/${data.answerId}`;

    return this.http.get(url).pipe(
      map((answer: IConfigAnswer) => {
        return <IConfigAnswer>{
          ...answer,
          id: data.answerId
        };
      })
    );
  }

  getAnswerDisabledStatus(data: { modelId: string, questionId: string, answer: IConfigAnswer, selectedConfiguration: { [key: string]: string[]; } }) {
    const url = `${this.API_ENDPOINT}/${data.modelId}/questions/${data.questionId}/answers/${data.answer.id}/status`;
    const payload = {
      selection: {
        ...data.selectedConfiguration
      }
    };

    return this.http.post(url, payload).pipe(
      map((response: { reasons: IAnswerReason[] }) => {
        let reasonRes = 'Disabled by selection of: \n';

        response.reasons.map((reason, index) => {
          if (index > 0) {
            reasonRes += ', ' + reason.displayName + ' ';
          } else {
            reasonRes += ' ' + reason.displayName + ' ';
          }
        });

        return reasonRes;
      })
    );
  }
}
