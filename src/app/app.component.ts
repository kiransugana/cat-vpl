import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { select, Store } from '@ngrx/store';
import * as fromState from './state';

@Component({
  selector: 'cat-vpl',
  templateUrl: './app.component.html'
})
export class AppComponent {
  private pageTitle$ = this.store.pipe(
    select(fromState.$getPageTitle)
  );

  constructor(private titleService: Title,
              private store: Store<fromState.RouterStateUrl>) {
    this.pageTitle$.subscribe((title: string) => {
      titleService.setTitle(title);
    });
  }
}
