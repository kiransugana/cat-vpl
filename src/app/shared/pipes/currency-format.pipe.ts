import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { CatVplUtils } from '../../util';
import { formatCurrency } from '@angular/common';

@Pipe({
  name: 'currencyFormat'
})
export class CurrencyFormatPipe implements PipeTransform {

  constructor(@Inject(LOCALE_ID) private locale: string) {
  }

  transform(value: string, currency: string = '$', isIncluded: boolean = false): string {
    if (isIncluded) {
      return 'Included';
    } else if (value === '0.00') {
      return 'NC';
    } else {
      const num = CatVplUtils.strToNumber(value);
      return formatCurrency(num, this.locale, currency);
    }
  }
}
