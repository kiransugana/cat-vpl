import { NgModule } from '@angular/core';
import { BannerComponent } from './components/banner/banner.component';
import { CurrencyFormatPipe } from './pipes/currency-format.pipe';

@NgModule({
  imports: [],
  declarations: [BannerComponent, CurrencyFormatPipe],
  exports: [BannerComponent, CurrencyFormatPipe]
})
export class SharedModule {
}
