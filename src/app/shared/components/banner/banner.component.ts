import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html'
})
export class BannerComponent {
  @Input() preText: string;
  @Input() bannerTitle: string;
  @Input() styleClass: string;
}
