import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  bannerTitle = 'CONFIGURE A MACHINE IN MINUTES';
  bannerPreText = 'WELCOME TO CAT FEATURE BASED CONFIGURATOR LITE';

  standardConfigTitle = 'STANDARD CONFIGURATIONS';
  standardConfigDesc = 'Choose from a saved standard dealer configuration as a starting point. You will have the ability to modify it once loaded.';

  previousConfigTitle = 'PREVIOUS PROJECTS / ORDERS';
  previousConfigDesc = 'Choose from a saved standard dealer configuration as a starting point. You will have the ability to modify it once loaded.';

  constructor() {
  }

  ngOnInit() {
  }

}
