import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'configuration-results',
  templateUrl: './configuration-results.component.html'
})
export class ConfigurationResultsComponent implements OnInit {
  @Input() title: string;
  @Input() description: string;

  @ViewChild('imageSrcTpl') imageSrcTpl: TemplateRef<any>;
  @ViewChild('projectNameTpl') projectNameTpl: TemplateRef<any>;
  @ViewChild('startProjectTpl') startProjectTpl: TemplateRef<any>;

  rows = [
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x1', projectNumber: '123aa', model: '24M'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x2', projectNumber: '123dd', model: 'D10T'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x3', projectNumber: '123bb', model: '908M'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x4', projectNumber: '123bb', model: '908M'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x5', projectNumber: '123bb', model: '908M'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x5', projectNumber: '123bb', model: '908M'},
    {imageSrc: '/assets/images/placeholders/24M.png', projectName: '123x5', projectNumber: '123bb', model: '908M'},
  ];
  columns = [];

  ngOnInit() {
    this.columns = [
      {
        cellTemplate: this.imageSrcTpl,
        headerTemplate: '',
        name: 'imageSrc'
      },
      {
        name: 'Project Name',
        cellTemplate: this.projectNameTpl,
      },
      {name: 'Project Number'},
      {
        cellTemplate: this.startProjectTpl,
        headerTemplate: '',
        name: 'imageSrc'
      }
    ];
  }

  goToPage(event) {
    // todo:
  }

  selectRow(event) {
    // todo:
  }
}
