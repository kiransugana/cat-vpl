import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SharedModule } from '../../shared/shared.module';
import { ConfigurationSearchComponent } from './configuration-search/configuration-search.component';
import { ConfigurationResultsComponent } from './configuration-results/configuration-results.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [HomeComponent, ConfigurationSearchComponent, ConfigurationResultsComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    FontAwesomeModule,
    NgxDatatableModule
  ]
})
export class HomeModule { }
