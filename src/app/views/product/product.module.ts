import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { ConfigurationStepComponent } from './configuration-step/configuration-step.component';
import { PricingComponent } from './pricing/pricing.component';
import { ReferenceDocumentsComponent } from './reference-documents/reference-documents.component';
import { HeroComponent } from './hero/hero.component';
import { ConfigurationListComponent } from './configuration-list/configuration-list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OptionModalComponent } from './option-modal/option-modal.component';
import { ProductGuard } from './product.guard';
import { SharedModule } from '../../shared/shared.module';
import { ConfigurationOptionComponent } from './configuration-option/configuration-option.component';
import { TooltipModule } from 'ng2-tooltip-directive';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';

@NgModule({
  declarations: [
    ProductComponent,
    ConfigurationStepComponent,
    ConfigurationListComponent,
    PricingComponent,
    ReferenceDocumentsComponent,
    HeroComponent,
    OptionModalComponent,
    ConfigurationOptionComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    FontAwesomeModule,
    SharedModule,
    TooltipModule,
    AngularStickyThingsModule
  ],
  providers: [
    ProductGuard
  ]
})
export class ProductModule {
}
