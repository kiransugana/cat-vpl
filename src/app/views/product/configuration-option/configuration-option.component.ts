import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IConfigAnswer, IConfigQuestion } from '../../../models/configuration';
import { CustomValidators } from '../../../util/validators/custom.validators';

@Component({
  selector: 'configuration-option',
  templateUrl: './configuration-option.component.html'
})
export class ConfigurationOptionComponent implements OnInit {
  @Input() question: IConfigQuestion;

  @Output() configurationChange = new EventEmitter<{ question: IConfigQuestion, answer: IConfigAnswer }>();
  @Output() answerDetail = new EventEmitter<{ questionId: string, answer: IConfigAnswer }>();
  @Output() answerDisabledStatus = new EventEmitter<{ questionId: string, answer: IConfigAnswer }>();

  constructor() {
  }

  ngOnInit() {
  }

  selectOption(question: IConfigQuestion, answer: IConfigAnswer) {
    if (CustomValidators.QuestionValidator({question, answer})) {
      this.configurationChange.emit({
        question,
        answer
      });
    }
  }

  getAnswerDetails(questionId: string, answer: IConfigAnswer) {
    this.answerDetail.emit({
      questionId,
      answer
    });
  }

  displayStatus(questionId: string, answer: IConfigAnswer) {
    if (answer.isDisabled && !answer.isIncluded && !answer.status) {
      this.answerDisabledStatus.emit({
        questionId,
        answer
      });
    }
  }
}
