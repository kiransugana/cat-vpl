import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product.component';
import { ProductGuard } from './product.guard';

const routes: Routes = [
  {
    path: ':productId',
    component: ProductComponent,
    canActivate: [ProductGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
