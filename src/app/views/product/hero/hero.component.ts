import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfigurationStepType } from '../../../models/configuration/step.type';

@Component({
  selector: 'product-hero',
  templateUrl: './hero.component.html'
})
export class HeroComponent implements OnInit {
  @Input() currentStep: ConfigurationStepType | number;
  @Input() configurationValid: { valid: boolean, firstInvalidAnchor: string; };

  @Output() goToStep = new EventEmitter<number>();
  @Output() invalidConfig = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
  }

  nextStep() {
    if (this.configurationValid.valid) {
      this.goToStep.emit(3);
    } else {
      this.invalidConfig.emit(this.configurationValid.firstInvalidAnchor);
    }
  }

  previousStep() {
    this.goToStep.emit(1);
  }
}
