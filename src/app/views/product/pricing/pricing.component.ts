import { Component, Input, OnInit } from '@angular/core';
import { IConfigAnswer } from '../../../models/configuration';

@Component({
  selector: 'configuration-pricing',
  templateUrl: './pricing.component.html'
})
export class PricingComponent implements OnInit {
  @Input() pricing: {id: string, selections: IConfigAnswer[], totalPrice: number};

  constructor() {
  }

  ngOnInit() {
  }

}
