import { Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { isNull } from 'util';
import { IConfigAnswer, IConfigQuestion } from '../../../models/configuration';
import { IPendingUpdate } from '../../../models/configuration/pending-update';

@Component({
  selector: 'option-modal',
  templateUrl: './option-modal.component.html'
})
export class OptionModalComponent implements OnChanges {
  @Input() updateOptions: IPendingUpdate;

  @Output() modalClose = new EventEmitter<{questions: IConfigQuestion[], answers: IConfigAnswer[], answerIds: string[], questionIds: string[]}>();

  showModal: boolean;

  constructor(private elementRef: ElementRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.updateOptions.currentValue) {
      this.showModal = !isNull(changes.updateOptions.currentValue);
      this.elementRef.nativeElement.ownerDocument.body.style.overflow = this.showModal ? 'hidden' : 'auto';
    }
  }

  closeModal(value: any, event?) {
    if (event && event.target.className !== 'modal-backdrop') {
      return;
    }
    this.showModal = false;
    this.elementRef.nativeElement.ownerDocument.body.style.overflow = 'auto';
    this.modalClose.emit(value);
  }

  confirmUpdate(updateOptions: { toRemove: { question: string, answer: string }[], toAdd: { question: string, answer: string }[], questions: IConfigQuestion[], answers: IConfigAnswer[] }) {
    const questions: IConfigQuestion[] = Object.assign([], updateOptions.questions);

    questions.map(question => {
      const qs = Object.assign({}, question);
      updateOptions.toRemove.map((item: { question: string, answer: string }) => {
        if (qs.id === item.question) {
          qs.answerIds = Object.assign([], qs.answerIds.filter(aId => aId !== item.answer));
        }
      });
      updateOptions.toAdd.map((item: { question: string, answer: string }) => {
        if (question.id === item.question && !qs.answerIds.includes(item.answer)) {
          qs.answerIds.push(item.answer);
        }
      });

      return qs;
    });

    this.closeModal({questions, answers: updateOptions.answers, answerIds: this.updateOptions.answerIds, questionIds: this.updateOptions.questionIds});
  }
}
