import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IConfigAnswer, IConfigQuestion, IConfigurationDetail } from '../../../models/configuration';

@Component({
  selector: 'configuration-list',
  templateUrl: './configuration-list.component.html'
})
export class ConfigurationListComponent implements OnInit {
  @Input() productConfiguration: IConfigurationDetail;

  @Output() configurationChange = new EventEmitter<{ modelId: string, question: IConfigQuestion, answer: IConfigAnswer }>();
  @Output() answerDetail = new EventEmitter<{ modelId: string, questionId: string, answer: IConfigAnswer }>();
  @Output() answerDisabled = new EventEmitter<{ modelId: string, questionId: string, answer: IConfigAnswer }>();

  constructor() {
  }

  ngOnInit() {
  }

  emitAnswerDetail(event: { questionId: string, answer: IConfigAnswer }) {
    this.answerDetail.emit({
      ...event,
      modelId: this.productConfiguration.id
    });
  }

  emitConfigurationChange(event: { question: IConfigQuestion, answer: IConfigAnswer }) {
    this.configurationChange.emit({
      ...event,
      modelId: this.productConfiguration.id
    });
  }

  emitAnswerDisabledStatus(event: { questionId: string, answer: IConfigAnswer }) {
    this.answerDisabled.emit({
      ...event,
      modelId: this.productConfiguration.id
    });
  }
}
