import { Component, Input, OnInit } from '@angular/core';
import { IConfigurationStep } from '../../../models';

@Component({
  selector: 'configuration-step',
  templateUrl: './configuration-step.component.html'
})
export class ConfigurationStepComponent implements OnInit {
  @Input() activeStep = 1;

  configuratinSteps: IConfigurationStep[] = [
    {
      label: 'Configuration',
      icon: ''
    },
    {
      label: 'Attachments/Tools',
      icon: ''
    },
    {
      label: 'Pricing/Financing',
      icon: ''
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
