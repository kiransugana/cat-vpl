import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRouteSnapshot, CanActivate } from '@angular/router';
import { Observable, of } from 'rxjs';
import * as fromState from '../../state';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { IProductConfig } from '../../models/configuration';

@Injectable()
export class ProductGuard implements CanActivate {
  constructor(private store$: Store<fromState.ProductState>) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.configurationExists(route.params.productId).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  configurationExists(id: string): Observable<{ [key: string]: IProductConfig; }> {
    return this.store$
      .pipe(
        select(fromState.$getProductConfigEntities),
        tap((entities: { [key: string]: IProductConfig }) => {
          if (!entities[id]) {
            this.store$.dispatch(new fromState.LoadProductConfiguration(id));
          } else {
            this.store$.dispatch(new fromState.SelectActiveConfiguration(id));
          }
        }),
        filter((entities: { [key: string]: IProductConfig }) => !!entities[id]),
        take(1)
      );
  }
}
