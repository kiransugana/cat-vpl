import { Component, OnInit } from '@angular/core';
import { IReferenceDocumentCategory } from '../../../models/reference-document/category';

@Component({
  selector: 'reference-documents',
  templateUrl: './reference-documents.component.html'
})
export class ReferenceDocumentsComponent implements OnInit {
  referenceDocumentsList: IReferenceDocumentCategory[] = [
    {
      title: 'COMPETITIVE SPECS',
      hasMore: true,
      referenceDocuments: [
        {
          icon: 'web',
          label: 'Spec Check Home'
        },
        {
          icon: 'pdf',
          label: 'Deere 770G/GP'
        },
        {
          icon: 'pdf',
          label: 'Deere 772G/GP'
        },
        {
          icon: 'pdf',
          label: 'Komatsu GD 655-6'
        }
      ]
    },
    {
      title: 'SELLING TIPS',
      referenceDocuments: [
        {
          icon: 'pdf',
          label: 'Deere 772G / GP'
        },
        {
          icon: 'pdf',
          label: 'Komatsu'
        },
        {
          icon: 'pdf',
          label: 'CNH 865B AWD'
        }
      ]
    },
    {
      title: 'KEY FEATURES',
      referenceDocuments: [
        {
          icon: 'pdf',
          label: '12M3 AWD Specalog'
        },
        {
          icon: 'pdf',
          label: 'Application Guide'
        }
      ]
    },
    {
      title: 'TECHNOLOGY',
      referenceDocuments: [
        {
          icon: 'yt',
          label: 'M3 Tech Overview'
        },
        {
          icon: 'yt',
          label: 'Adv. Control Joysticks'
        },
        {
          icon: 'yt',
          label: 'Auto Articulation'
        },
        {
          icon: 'yt',
          label: 'Stable Blade 1'
        },
        {
          icon: 'yt',
          label: 'Stable Blade 2'
        }
      ]
    },
    {
      title: 'TRAINING',
      referenceDocuments: [
        {
          icon: 'yt',
          label: 'M3 Sales Training'
        },
        {
          icon: 'yt',
          label: 'Grade Control Training'
        },
        {
          icon: 'yt',
          label: 'Demo Tips'
        },
        {
          icon: 'yt',
          label: 'M2 Walkaround'
        }
      ]
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
