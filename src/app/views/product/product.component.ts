import { ChangeDetectionStrategy, Component, ElementRef } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromState from '../../state';
import { Observable } from 'rxjs';
import { IConfigAnswer, IConfigQuestion, IConfigurationDetail } from '../../models/configuration';
import { isNull } from 'util';
import { ConfigurationStepType } from '../../models/configuration/step.type';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductComponent {
  stepTitle = 'Options Configuration';
  step: ConfigurationStepType | number = ConfigurationStepType.CONFIGURATION; // todo: add step in Store

  productConfiguration$: Observable<IConfigurationDetail> = this.store$.select(fromState.$getProductConfiguration);

  configurationValid$ = this.store$.select(fromState.$getProductConfigurationValidity);

  configurationUpdate$ = this.store$.select(fromState.$getProductConfigUpdatePendingDetails);

  configurationPricing$ = this.store$.select(fromState.$getProductConfigurationPricing);

  constructor(private store$: Store<fromState.ProductState>,
              private el: ElementRef) {
  }

  getAnswerDetail(event: { modelId: string, questionId: string, answer: IConfigAnswer }) {
    this.store$.dispatch(new fromState.GetAnswerDetail(event));
  }

  getAnswerDisabledStatus(event: { modelId: string, questionId: string, answer: IConfigAnswer }) {
    this.store$.dispatch(new fromState.GetAnswerDisabledStatus(event));
  }

  updateConfiguration(event: { modelId: string, question: IConfigQuestion, answer: IConfigAnswer }) {
    this.store$.dispatch(new fromState.UpdateConfiguration(event));
  }

  handleConfigurationUpdate(event: { questions: IConfigQuestion[], answers: IConfigAnswer[], answerIds: string[], questionIds: string[] }) {
    if (isNull(event)) {
      this.store$.dispatch(new fromState.UpdateConfigurationCancel());
    } else {
      this.store$.dispatch(new fromState.UpdateConfigurationSuccess(event));
    }
  }

  nextStep(event: number) {
    this.step = event;
    this.stepTitle = event === 1 ? 'Options Configuration' : 'Pricing'; // todo: move stepper to store
  }

  scrollToInvalidQuestion(event: string) {
    this.el.nativeElement.querySelector('#qs-' + event).scrollIntoView({behavior: 'smooth', block: 'center'});
  }
}
