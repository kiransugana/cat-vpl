import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createSelector } from '@ngrx/store';

import * as fromProductConfiguration from '../actions/product-configuration.action';
import { IProductConfig } from '../../models';
import { IPendingUpdate } from '../../models/configuration/pending-update';

// Adapter
export const productConfigAdapter: EntityAdapter<IProductConfig> = createEntityAdapter<IProductConfig>();

// State
export interface ProductConfigurationState extends EntityState<IProductConfig> {
  activeProductConfigId: string | null;
  loaded: boolean;
  loading: boolean;
  error: any;
  pendingUpdate: IPendingUpdate;
  updateCount: number;
}

const initialState: ProductConfigurationState = productConfigAdapter.getInitialState({
  activeProductConfigId: null,
  loaded: false,
  loading: false,
  error: null,
  pendingUpdate: null,
  updateCount: 0
});

export function productConfigurationReducer(state = initialState, action: fromProductConfiguration.ProductConfigurationAction): ProductConfigurationState {
  switch (action.type) {
    case fromProductConfiguration.UPDATE_CONFIGURATION:
    case fromProductConfiguration.LOAD_PRODUCT_CONFIGURATION: {
      return {
        ...state,
        loading: true,
        loaded: false,
        error: null
      };
    }

    case fromProductConfiguration.LOAD_PRODUCT_CONFIGURATION_SUCCESS: {
      return productConfigAdapter.addOne(action.payload.config, {
        ...state,
        loading: false,
        loaded: true,
        error: null,
        activeProductConfigId: action.payload.config.id,
        updateCount: 0
      });
    }

    case fromProductConfiguration.UPDATE_CONFIGURATION_PENDING: {
      return {
        ...state,
        pendingUpdate: action.payload
      };
    }

    case fromProductConfiguration.UPDATE_CONFIGURATION_SUCCESS:
    case fromProductConfiguration.UPDATE_CONFIGURATION_CANCEL: {
      return {
        ...state,
        pendingUpdate: null,
        updateCount: state.updateCount + 1
      };
    }

    case fromProductConfiguration.SELECT_ACTIVE_CONFIGURATION: {
      return {
        ...state,
        activeProductConfigId: action.payload
      };
    }

    case fromProductConfiguration.LOAD_PRODUCT_CONFIGURATION_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getActiveProductConfigId = (state: ProductConfigurationState) => state.activeProductConfigId;
export const getProductConfigLoading = (state: ProductConfigurationState) => state.loading;
export const getProductConfigLoaded = (state: ProductConfigurationState) => state.loaded;
export const getProductConfigError = (state: ProductConfigurationState) => state.error;
export const getProductConfigUpdatePending = (state: ProductConfigurationState) => state.pendingUpdate;
export const getProductConfigUpdateCount = (state: ProductConfigurationState) => state.updateCount;

const {
  selectEntities: productConfigurationEntities,
  selectAll: allProductConfigurations
} = productConfigAdapter.getSelectors();

export const selectProductConfigurationState = (state: ProductConfigurationState) => state;
export const selectAllProductConfigurations = createSelector(selectProductConfigurationState, allProductConfigurations);
export const selectProductConfigurationEntities = createSelector(selectProductConfigurationState, productConfigurationEntities);
export const selectActiveProductConfiguration = createSelector(
  selectProductConfigurationEntities,
  getActiveProductConfigId,
  (entityList, activeId) => {

    return entityList[activeId];
  }
);
