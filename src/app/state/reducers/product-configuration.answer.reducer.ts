import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createSelector } from '@ngrx/store';

import * as fromProductConfiguration from '../actions/product-configuration.action';
import { IConfigAnswer } from '../../models';

// Adapter
export const configAnswerAdapter: EntityAdapter<IConfigAnswer> = createEntityAdapter<IConfigAnswer>();

// State
export interface ConfigurationAnswerState extends EntityState<IConfigAnswer> {
}

const initialState: ConfigurationAnswerState = configAnswerAdapter.getInitialState();

export function configAnswerReducer(state = initialState, action: fromProductConfiguration.ProductConfigurationAction): ConfigurationAnswerState {
  switch (action.type) {

    case fromProductConfiguration.LOAD_PRODUCT_CONFIGURATION_SUCCESS: {
      return configAnswerAdapter.upsertMany(action.payload.answers, state);
    }

    case fromProductConfiguration.GET_ANSWER_DISABLED_STATUS_SUCCESS:
    case fromProductConfiguration.GET_ANSWER_DETAILS_SUCCESS: {
      return configAnswerAdapter.updateOne({
        id: action.payload.id,
        changes: action.payload
      }, state);
    }

    case fromProductConfiguration.UPDATE_CONFIGURATION_SUCCESS: {
      const resetDisabledState = configAnswerAdapter.updateMany(
        action.payload.answerIds.map((answerId) => Object.assign({}, {
          id: answerId, changes: {
            isDisabled: true
          }
        })), {
          ...state
        });

      return configAnswerAdapter.upsertMany(action.payload.answers, resetDisabledState);
    }

    default: {
      return state;
    }
  }
}

const {
  selectEntities: configurationAnswerEntities,
  selectAll: allConfigurationAnswers,
  selectTotal: configurationAnswerCount
} = configAnswerAdapter.getSelectors();

export const selectConfigurationAnswerState = (state: ConfigurationAnswerState) => state;
export const selectAllConfigurationAnswers = createSelector(selectConfigurationAnswerState, allConfigurationAnswers);
export const selectConfigurationAnswerEntities = createSelector(selectConfigurationAnswerState, configurationAnswerEntities);
export const selectConfigurationAnswerCount = createSelector(selectConfigurationAnswerState, configurationAnswerCount);
