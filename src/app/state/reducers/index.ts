import * as fromRouter from '@ngrx/router-store';
import * as fromProductConfiguration from './product-configuration.reducer';
import * as fromProductConfigurationQuestion from './product-configuration.question.reducer';
import * as fromProductConfigurationAnswer from './product-configuration.answer.reducer';

import { ActionReducerMap, createFeatureSelector, MetaReducer } from '@ngrx/store';
import { RouterStateUrl } from './router.reducer';
import { environment } from '../../../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';

export * from './router.reducer';
export * from './product-configuration.reducer';
export * from './product-configuration.answer.reducer';
export * from './product-configuration.question.reducer';

// meta reducers
export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

// router state
export const $getRouterState = createFeatureSelector<fromRouter.RouterReducerState<RouterStateUrl>>(
  'router'
);

// product state
export interface ProductState {
  config: fromProductConfiguration.ProductConfigurationState;
  questions: fromProductConfigurationQuestion.ConfigurationQuestionState;
  answers: fromProductConfigurationAnswer.ConfigurationAnswerState;
}

export const $getProductState = createFeatureSelector<ProductState>(
  'product'
);

export const productReducers: ActionReducerMap<ProductState> = {
  config: fromProductConfiguration.productConfigurationReducer,
  questions: fromProductConfigurationQuestion.configQuestionReducer,
  answers: fromProductConfigurationAnswer.configAnswerReducer
};

