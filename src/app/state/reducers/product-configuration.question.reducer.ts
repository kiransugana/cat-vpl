import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createSelector } from '@ngrx/store';

import * as fromProductConfiguration from '../actions/product-configuration.action';
import { IConfigQuestion } from '../../models';
import { configAnswerAdapter } from './product-configuration.answer.reducer';

// Adapter
export const configQuestionAdapter: EntityAdapter<IConfigQuestion> = createEntityAdapter<IConfigQuestion>();

// State
export interface ConfigurationQuestionState extends EntityState<IConfigQuestion> {
}

const initialState: ConfigurationQuestionState = configQuestionAdapter.getInitialState();

export function configQuestionReducer(state = initialState, action: fromProductConfiguration.ProductConfigurationAction): ConfigurationQuestionState {
  switch (action.type) {

    case fromProductConfiguration.LOAD_PRODUCT_CONFIGURATION_SUCCESS: {
      return configQuestionAdapter.upsertMany(action.payload.questions, state);
    }

    case fromProductConfiguration.UPDATE_CONFIGURATION_SUCCESS: {
      const resetDisabledState = configQuestionAdapter.updateMany(
        action.payload.questionIds.map((questionId) => Object.assign({}, {
          id: questionId, changes: {
            selected_value: []
          }
        })), {
          ...state
        });

      const questionsPayload = action.payload.questions.map((qs) => {
        const question = {
          ...qs
        };

        question.answerIds = state.entities[question.id] ? state.entities[question.id].answerIds : question.answerIds;

        return question;
      });

      return configQuestionAdapter.upsertMany(questionsPayload, resetDisabledState);
    }

    default: {
      return state;
    }
  }
}


const {
  selectEntities: configurationQuestionEntities,
  selectAll: allConfigurationQuestions,
  selectTotal: configurationQuestionCount
} = configQuestionAdapter.getSelectors();

export const selectConfigurationQuestionState = (state: ConfigurationQuestionState) => state;
export const selectAllConfigurationQuestions = createSelector(selectConfigurationQuestionState, allConfigurationQuestions);
export const selectConfigurationQuestionEntities = createSelector(selectConfigurationQuestionState, configurationQuestionEntities);
export const selectConfigurationQuestionCount = createSelector(selectConfigurationQuestionState, configurationQuestionCount);
