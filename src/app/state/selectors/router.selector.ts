import { createSelector } from '@ngrx/store';
import { $getRouterState } from '../reducers';

export const $getPageTitle = createSelector($getRouterState,
  (routerState) => {
    if (routerState) {
      return `${routerState.state.data.title} - VPL`;
    }
  }
);
