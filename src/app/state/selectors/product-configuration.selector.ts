import { createSelector } from '@ngrx/store';
import { $getProductState } from '../reducers';

import * as fromProductConfiguration from '../reducers/product-configuration.reducer';
import * as fromProductConfigurationQuestion from '../reducers/product-configuration.question.reducer';
import * as fromProductConfigurationAnswer from '../reducers/product-configuration.answer.reducer';
import { IConfigAnswer, IConfigQuestion, IConfigurationDetail, IProductConfig, SelectionType } from '../../models/configuration';
import { CatVplUtils } from '../../util';

export const $getProductConfigState = createSelector($getProductState, (state) => state.config);
export const $getProductConfigs = createSelector($getProductConfigState, fromProductConfiguration.selectAllProductConfigurations);
export const $getProductConfigEntities = createSelector($getProductConfigState, fromProductConfiguration.selectProductConfigurationEntities);
export const $getActiveProductConfig = createSelector($getProductConfigState, fromProductConfiguration.selectActiveProductConfiguration);

export const $getProductConfigLoading = createSelector($getProductConfigState, fromProductConfiguration.getProductConfigLoading);
export const $getProductConfigLoaded = createSelector($getProductConfigState, fromProductConfiguration.getProductConfigLoaded);
export const $getProductConfigError = createSelector($getProductConfigState, fromProductConfiguration.getProductConfigError);
export const $getProductConfigUpdatePending = createSelector($getProductConfigState, fromProductConfiguration.getProductConfigUpdatePending);
export const $getProductConfigUpdateCount = createSelector($getProductConfigState, fromProductConfiguration.getProductConfigUpdateCount);

export const $getConfigQuestionState = createSelector($getProductState, (state) => state.questions);
export const $getConfigQuestions = createSelector($getConfigQuestionState, fromProductConfigurationQuestion.selectAllConfigurationQuestions);
export const $getConfigQuestionEntities = createSelector($getConfigQuestionState, fromProductConfigurationQuestion.selectConfigurationQuestionEntities);
export const $getConfigQuestionCount = createSelector($getConfigQuestionState, fromProductConfigurationQuestion.selectConfigurationQuestionCount);

export const $getConfigAnswerState = createSelector($getProductState, (state) => state.answers);
export const $getConfigAnswers = createSelector($getConfigAnswerState, fromProductConfigurationAnswer.selectAllConfigurationAnswers);
export const $getConfigAnswerEntities = createSelector($getConfigAnswerState, fromProductConfigurationAnswer.selectConfigurationAnswerEntities);
export const $getConfigAnswerCount = createSelector($getConfigAnswerState, fromProductConfigurationAnswer.selectConfigurationAnswerCount);

export const $getProductConfiguration = createSelector($getActiveProductConfig, $getConfigQuestionEntities, $getConfigAnswerEntities,
  (config: IProductConfig, questions: { [key: string]: IConfigQuestion; }, answers: { [key: string]: IConfigAnswer; }) => {
    if (config && questions && answers) {
      const questionList = config.questionIds
        .map(questionId => {
          const question = {
            ...questions[questionId]
          };

          if (question.answerIds && question.answerIds.length > 0) {
            question.answers = question.answerIds.map(answerId => answers[answerId]);

            if (question.selection_type === SelectionType.MAY_SELECT_ONE) {
              question.tooltipText = 'May Select One';
            } else if (question.selection_type === SelectionType.MAY_SELECT_ONE_OR_MANY) {
              question.tooltipText = 'May Select One or Many';
            } else if (question.selection_type === SelectionType.MUST_SELECT_ONE) {
              question.tooltipText = 'Must Select One';
            } else if (question.selection_type === SelectionType.MUST_SELECT_ONE_OR_MANY) {
              question.tooltipText = 'Must Select One or Many';
            }

            return question;
          }
        });

      return <IConfigurationDetail> {
        id: config.id,
        questions: questionList,
        questionIds: config.questionIds,
        answerIds: config.answerIds
      };
    }
  }
);

export const $getProductConfigurationSelections = createSelector($getProductConfiguration, (productConfiguration) => {
  if (productConfiguration) {
    const selectedConfiguration = <{ [key: string]: string[]; }>{};

    productConfiguration.questions.map(question => {
      if (question) {
        selectedConfiguration[question.id] = question.selected_value;
      }
    });

    return {
      selectedConfiguration,
      answerIds: productConfiguration.answerIds,
      questionIds: productConfiguration.questionIds
    };
  }
});

export const $getProductConfigurationPricing = createSelector($getProductConfiguration, $getConfigAnswerEntities,
  (config: IConfigurationDetail, answers: { [key: string]: IConfigAnswer; }) => {
    if (config && answers) {
      const answerList = [];
      let totalPrice = 0;
      config.questions
        .filter(question => question && question.selected_value.length > 0)
        .map(filteredQuestion => {
          filteredQuestion.selected_value.map(answerId => {
            const answer = Object.assign({}, answers[answerId]);
            answerList.push(answer);
            totalPrice += CatVplUtils.strToNumber(answer.price);
          });
        });

      return <{ id: string, selections: IConfigAnswer[], totalPrice: number }>{
        id: config.id,
        selections: answerList,
        totalPrice
      };
    }
  }
);

export const $getProductConfigUpdatePendingDetails = createSelector($getActiveProductConfig, $getProductConfigUpdatePending, $getConfigQuestionEntities, $getConfigAnswerEntities,
  (productConfig, configUpdate, questions, answers) => {
    if (productConfig && configUpdate && questions && answers) {
      const toAdd = configUpdate.toAdd.map((questionAnswer) => {
        // todo: might not need to try entities?
        const question = questions[questionAnswer.question] || configUpdate.questions.find(qs => qs.id === questionAnswer.question);
        const answer = answers[questionAnswer.answer] || configUpdate.answers.find(ans => ans.id === questionAnswer.answer);

        return {
          question: question.displayName,
          answer: answer.displayName
        };
      });

      const toRemove = configUpdate.toRemove.map((questionAnswer) => {
        const question = questions[questionAnswer.question];
        const answer = answers[questionAnswer.answer];

        return {
          question: question.displayName,
          answer: answer.displayName
        };
      });

      return {
        ...configUpdate,
        toAdd,
        toRemove,
        answerIds: productConfig.answerIds,
        questionIds: productConfig.questionIds
      };
    }
  });

export const $getProductConfigurationValidity = createSelector($getActiveProductConfig, $getConfigQuestionEntities,
  (config, questions) => {
    const questionList = config.questionIds
      .map(questionId => Object.assign({}, questions[questionId]))
      .filter(question => question.selection_type === SelectionType.MUST_SELECT_ONE_OR_MANY || question.selection_type === SelectionType.MUST_SELECT_ONE)
      .filter(question => question.selected_value.length === 0);

    return {
      valid: questionList.length === 0,
      firstInvalidAnchor: questionList.length === 0 ? null : questionList[0].id
    };
  });
