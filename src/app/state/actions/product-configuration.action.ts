import { Action } from '@ngrx/store';
import { IConfigAnswer, IConfigQuestion, IProductConfig } from '../../models/configuration';
import { IPendingUpdate } from '../../models/configuration/pending-update';

export const LOAD_PRODUCT_CONFIGURATION = '[ProductConfiguration] Load';
export const LOAD_PRODUCT_CONFIGURATION_SUCCESS = '[ProductConfiguration] Load Success';
export const LOAD_PRODUCT_CONFIGURATION_FAIL = '[ProductConfiguration] Load Fail';

export const GET_ANSWER_DETAILS = '[ProductConfigAnswer] Get';
export const GET_ANSWER_DETAILS_SUCCESS = '[ProductConfigAnswer] Get Success';
export const GET_ANSWER_DETAILS_FAIL = '[ProductConfigAnswer] Get Fail';

export const GET_ANSWER_DISABLED_STATUS = '[ProductConfigAnswer] Get Disabled Status';
export const GET_ANSWER_DISABLED_STATUS_SUCCESS = '[ProductConfigAnswer] Get Disabled Status Success';
export const GET_ANSWER_DISABLED_STATUS_FAIL = '[ProductConfigAnswer] Get Disabled Status Fail';

export const UPDATE_CONFIGURATION = '[ProductConfiguration] Update Configuration';
export const UPDATE_CONFIGURATION_PENDING = '[ProductConfiguration] Update Configuration Pending';
export const UPDATE_CONFIGURATION_CANCEL = '[ProductConfiguration] Update Configuration Cancel';
export const UPDATE_CONFIGURATION_SUCCESS = '[ProductConfiguration] Update Configuration Success';
export const UPDATE_CONFIGURATION_FAIL = '[ProductConfiguration] Update Configuration Fail';

export const SELECT_ACTIVE_CONFIGURATION = '[ProductConfiguration] Select Active';

// load product configuration
export class LoadProductConfiguration implements Action {
  readonly type = LOAD_PRODUCT_CONFIGURATION;

  constructor(public payload: string) {
  }
}

export class LoadProductConfigurationSuccess implements Action {
  readonly type = LOAD_PRODUCT_CONFIGURATION_SUCCESS;

  constructor(public payload: { questions: IConfigQuestion[], answers: IConfigAnswer[], config: IProductConfig }) {
  }
}

export class LoadProductConfigurationFail implements Action {
  readonly type = LOAD_PRODUCT_CONFIGURATION_FAIL;

  constructor(public payload: any) {
  }
}

export class UpdateConfiguration implements Action {
  readonly type = UPDATE_CONFIGURATION;

  constructor(public payload: { modelId: string, question: IConfigQuestion, answer: IConfigAnswer }) {
  }
}

export class UpdateConfigurationPending implements Action {
  readonly type = UPDATE_CONFIGURATION_PENDING;

  constructor(public payload: IPendingUpdate) {
  }
}

export class UpdateConfigurationCancel implements Action {
  readonly type = UPDATE_CONFIGURATION_CANCEL;
}

export class UpdateConfigurationSuccess implements Action {
  readonly type = UPDATE_CONFIGURATION_SUCCESS;

  constructor(public payload: { questions: IConfigQuestion[], answers: IConfigAnswer[], answerIds: string[], questionIds: string[] }) {
  }
}

export class UpdateConfigurationFail implements Action {
  readonly type = UPDATE_CONFIGURATION_FAIL;

  constructor(public payload: any) {
  }
}

export class SelectActiveConfiguration implements Action {
  readonly type = SELECT_ACTIVE_CONFIGURATION;

  constructor(public payload: string) {
  }
}

// answers
export class GetAnswerDetail implements Action {
  readonly type = GET_ANSWER_DETAILS;

  constructor(public payload: { modelId: string, questionId: string, answer: IConfigAnswer }) {
  }
}

export class GetAnswerDetailSuccess implements Action {
  readonly type = GET_ANSWER_DETAILS_SUCCESS;

  constructor(public payload: IConfigAnswer) {
  }
}

export class GetAnswerDetailFail implements Action {
  readonly type = GET_ANSWER_DETAILS_FAIL;

  constructor(public payload: any) {
  }
}

export class GetAnswerDisabledStatus implements Action {
  readonly type = GET_ANSWER_DISABLED_STATUS;

  constructor(public payload: { modelId: string, questionId: string, answer: IConfigAnswer }) {
  }
}

export class GetAnswerDisabledStatusSuccess implements Action {
  readonly type = GET_ANSWER_DISABLED_STATUS_SUCCESS;

  constructor(public payload: IConfigAnswer) {
  }
}

export class GetAnswerDisabledStatusFail implements Action {
  readonly type = GET_ANSWER_DISABLED_STATUS_FAIL;

  constructor(public payload: any) {
  }
}

export type ProductConfigurationAction
  = LoadProductConfiguration
  | LoadProductConfigurationSuccess
  | LoadProductConfigurationFail
  | UpdateConfiguration
  | UpdateConfigurationSuccess
  | UpdateConfigurationFail
  | UpdateConfigurationPending
  | UpdateConfigurationCancel
  | SelectActiveConfiguration
  | GetAnswerDetail
  | GetAnswerDetailSuccess
  | GetAnswerDetailFail
  | GetAnswerDisabledStatus
  | GetAnswerDisabledStatusSuccess
  | GetAnswerDisabledStatusFail;
