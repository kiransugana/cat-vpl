import { RouterEffects } from './router.effect';
import { ProductConfigurationEffects } from './product-configuration.effect';

export * from './router.effect';
export * from './product-configuration.effect';

// app effects
export const $appEffects: any[] = [RouterEffects];

// product effects
export const $productEffects: any[] = [ProductConfigurationEffects];
