import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import * as productConfigurationActions from '../actions/product-configuration.action';
import * as fromSelectors from '../selectors';
import { from, of } from 'rxjs';
import { IConfigAnswer, IConfigQuestion, IProductConfig } from '../../models/configuration';
import { catchError, map, mergeMap, switchMap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProductService } from '../../providers/services/product.service';
import { Store } from '@ngrx/store';
import * as fromState from '../index';
import { IQuestionAnswer } from '../../models/configuration/question-answer';

@Injectable()
export class ProductConfigurationEffects {

  @Effect() loadProductConfiguration$ = this.actions$.pipe(
    ofType(productConfigurationActions.LOAD_PRODUCT_CONFIGURATION),
    map((action: productConfigurationActions.LoadProductConfiguration) => action.payload),
    mergeMap((productId) => {
      return this.productService.getProductConfiguration(productId).pipe(
        map((data: { config: IProductConfig, questions: IConfigQuestion[], answers: IConfigAnswer[] }) => new productConfigurationActions.LoadProductConfigurationSuccess(data)),
        catchError((err) => {
          this.router.navigate(['/']);
          return of(new productConfigurationActions.LoadProductConfigurationFail(err));
        })
      );
    })
  );

  @Effect() getAnswerDetails$ = this.actions$.pipe(
    ofType(productConfigurationActions.GET_ANSWER_DETAILS),
    map((action: productConfigurationActions.GetAnswerDetail) => action.payload),
    mergeMap((data: { modelId: string, questionId: string, answer: IConfigAnswer }): any => {
      if (!data.answer.detailsLoaded) {
        return this.productService.getAnswerDetails({
          modelId: data.modelId,
          questionId: data.questionId,
          answerId: data.answer.id
        }).pipe(
          map((configAnswer: IConfigAnswer) => new productConfigurationActions.GetAnswerDetailSuccess({
            ...configAnswer,
            detailsLoaded: true,
            expandDetails: true
          })),
          catchError((err) => of(new productConfigurationActions.GetAnswerDetailFail(err)))
        );
      } else {
        return of(new productConfigurationActions.GetAnswerDetailSuccess({
          ...data.answer,
          expandDetails: !data.answer.expandDetails
        }));
      }
    })
  );

  @Effect() getAnswerDisabledStatus$ = this.actions$.pipe(
    ofType(productConfigurationActions.GET_ANSWER_DISABLED_STATUS),
    map((action: productConfigurationActions.GetAnswerDisabledStatus) => action.payload),
    withLatestFrom(this.store$.select(fromSelectors.$getProductConfigurationSelections)),
    mergeMap(([data, storeData]) => {
      return this.productService.getAnswerDisabledStatus(
        <{ modelId: string, questionId: string, answer: IConfigAnswer, selectedConfiguration: { [key: string]: string[]; } }>
          {...data, selectedConfiguration: storeData.selectedConfiguration}).pipe(
        map((answerStatus: string) => new productConfigurationActions.GetAnswerDisabledStatusSuccess({
          ...data.answer,
          status: answerStatus
        })),
        catchError((err) => of(new productConfigurationActions.GetAnswerDisabledStatusFail(err)))
      );
    })
  );

  @Effect() updateConfiguration$ = this.actions$.pipe(
    ofType(productConfigurationActions.UPDATE_CONFIGURATION),
    map((action: productConfigurationActions.UpdateConfiguration) => action.payload),
    withLatestFrom(this.store$.select(fromSelectors.$getProductConfigurationSelections)),
    mergeMap(([configurationData, storeData]) => {
      return this.productService.updateProductConfiguration(<{ modelId: string, question: IConfigQuestion, answer: IConfigAnswer, selectedConfiguration: { [key: string]: string[]; } }>{
        ...configurationData,
        selectedConfiguration: storeData.selectedConfiguration
      }).pipe(
        map((data: { questions: IConfigQuestion[], answers: IConfigAnswer[], toAdd: IQuestionAnswer[], toRemove: IQuestionAnswer[] }) => {
          if (data.toAdd.length === 0 && data.toRemove.length === 0) {
            return new productConfigurationActions.UpdateConfigurationSuccess({
              questions: data.questions,
              answers: data.answers,
              answerIds: storeData.answerIds,
              questionIds: storeData.questionIds
            });
          } else {
            return new productConfigurationActions.UpdateConfigurationPending({
              ...data,
              question: configurationData.question
            });
          }
        }),
        catchError((err) => of(new productConfigurationActions.UpdateConfigurationFail(err)))
      );
    })
  );

  constructor(private actions$: Actions,
              private router: Router,
              private store$: Store<fromState.ProductState>,
              private productService: ProductService) {
  }
}
